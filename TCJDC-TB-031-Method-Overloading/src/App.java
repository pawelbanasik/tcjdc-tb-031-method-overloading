
public class App {

	public static void main(String[] args) {

		int newScore = calculateScore("Tim", 500);
		System.out.println("New score is " + newScore);
		calculateScore(75);
		calculateScore();
		calcFeetandInchesToCentimeters(50, 11);
		calcFeetandInchesToCentimeters(100);
	

	}

	public static int calculateScore(String playerName, int score) {

		System.out.println("Player " + playerName + " scored " + score + " points.");
		return score * 1000;
	}

	public static int calculateScore(int score) {

		System.out.println("Unnamed player scored " + score + " points.");
		return score * 1000;
	}

	public static int calculateScore() {

		System.out.println("No player, no player score.");
		return 0;
	}

	public static double calcFeetandInchesToCentimeters(double feet, double inches) {

		if (feet < 0 || inches < 0 || inches > 12) {
			return -1;
		} else {
			double centimeters = (30.48 * feet) + (2.54 * inches);
			System.out.println("Feet: " + feet + " + Inches: " + inches + " = " + centimeters + " cm");
			return centimeters;
		}

	}

	public static double calcFeetandInchesToCentimeters(double inches) {

		if (inches >= 0) {
			double feet = (int)inches / 12;
			double remainingInches = (int)inches % 12;
			
			return calcFeetandInchesToCentimeters(feet, remainingInches);
		} else {
			return -1;
		}

	}

}
